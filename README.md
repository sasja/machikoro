# machikoro

## wut?

a machikoro AI competition framework, run [test.py](test.py) to see some action

## machiwut?

its a cool japanese boardgame tpump brought to my attention, here, have a look at the [rules](http://idwgames.com/wp-content/uploads/2015/02/Machi-RULES-reduced.pdf)

## design
have a look at the [design](docs/design.md) and [bot api](docs/botapi.md) documents. its still a moving target tho.

## i want to compete
great! entering the competition should be as easy as forking a template repo an registering your bot. however we're not there yet :(

## looks like a neat project, i want to help
even better, lots to do! talk to sasja. it would be cool if you could help get something running by [newline](https://hackerspace.gent/newline/2017/). skills that might come in handy:

* python
* webdesign
* docker
* game ai
* any language skill for writing bot templates
* general software architecture
* github webhooks api
* behavioral psychology (or how to make it addictive!)
* markdown :)
